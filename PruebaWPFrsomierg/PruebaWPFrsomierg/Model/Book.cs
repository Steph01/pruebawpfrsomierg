﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using PruebaWPFrsomierg.Modelo;
using PruebaWPFrsomierg.Interfaces;

namespace PruebaWPFrsomierg.Model
{
    [Serializable]
    public class Book : IToFile, IFromFile<Book>
    {
        public string Año { get; set; }
        public string categoria { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }

        public Book FromXml(string filepath)
        {
            throw new NotImplementedException();
        }
        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }
    }
}
